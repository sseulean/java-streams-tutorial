package com.tutorial.streams;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Streams are Monads, thus playing a big part in bringing functional programming to Java.
 *
 * In functional programming, a monad is a structure that represents computations defined as sequences of steps.
 * A type with a monad structure defines what it means to chain operations, or nest functions of that type together.

 */
public class StreamsDemo {

    public static void main(String args[]) {


        BigDecimal bigDecimal1=new BigDecimal(100);
        BigDecimal bigDecimal2=new BigDecimal(200);
        System.out.println(bigDecimal1.negate());
        boolean a=bigDecimal1.add(bigDecimal2.negate()).compareTo(BigDecimal.ZERO)<0;
        System.out.println(a);
    }

    private static List<String> filterElements() {
        List<String> filteredElements = new ArrayList<>();
        List<String> myList =
                Arrays.asList("a1", "a2", "b1", "c2", "c1");
        for (String s : myList) {
            if (s.startsWith("c")) {
                filteredElements.add(s.toUpperCase());
            }
        }

        return filteredElements;
    }

    private static void sequentialStreamsExample(){

        //Stream can be created from different data structures  especially from collections(arrays and list.)
        //From list and sets we can create different kind of stream:sequential streams and parallel streams
//        Arrays.asList("a1", "a2", "a3")
//                .stream()
//                .findFirst()
//                .ifPresent(System.out::println);

//        //Stream of objects.It is not necessary to create collections in order to work with Stream API from Java
//        //Use stream of to create a stream from a bunch of objects and to apply different operations on it.
//        Stream.of("a1", "a2", "a3")
//                .findFirst()
//                .ifPresent(System.out::println);
//
//        //Beside stream  of object in Java 8 we have streams for regular data type such as int type,long type and double type
//        IntStream.range(1, 4)
//                .forEach(System.out::println);
//
//        LongStream.range(1,4).forEach(System.out::println);
//
//        //Some other example of streams
//        //And primitive streams support the additional terminal aggregate operations sum() and average()
//        Arrays.stream(new int[] {1, 2, 3})
//                .map(n -> 2 * n + 1)
//                .average()
//                .ifPresent(System.out::println);//5.0
//
//        //Sometimes it's useful to transform a regular object stream to a primitive stream or vice versa.\
//
//        //object  stream transformed to primitive stream
//        Stream.of("a1", "a2", "a3")
//                .map(s -> s.substring(1))
//                .mapToInt(Integer::parseInt)
//                .max()
//                .ifPresent(System.out::println);
//
//        //vice versa:primitive stream transformed to object streams
//        IntStream.range(1, 4)
//                .mapToObj(i -> "a" + i)
//                .forEach(System.out::println);
//
//        //combined example
        Stream.of(1.0, 2.0, 3.0)
                .mapToInt(Double::intValue)
                .mapToObj(i -> "a" + i)
                .forEach(System.out::println);


    }
}
