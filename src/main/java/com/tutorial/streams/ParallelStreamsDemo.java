package com.tutorial.streams;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;

/**
 * Streams can be executed in parallel to increase runtime performance on large amount of input elements.
 * Parallel streams use a common ForkJoinPool available via the static ForkJoinPool.commonPool() method.
 * The size of the underlying thread-pool uses up to five threads - depending on the amount of available physical CPU cores.
 */
public class ParallelStreamsDemo {

    public static void main(String args[]) {
        System.out.println("Hello parallel streams !!!");
        //getParallelStreamsUnderlyingThreadPool();
        parallelStreamExecutionExample();


    }

    private static void getParallelStreamsUnderlyingThreadPool(){
        ForkJoinPool commonPool = ForkJoinPool.commonPool();
        System.out.println(commonPool.getParallelism());
    }

    /**
     * Collections support the method parallelStream() to create a parallel stream of elements.
     * Alternatively you can call the intermediate method parallel() on a given stream to convert a sequential stream to a parallel counterpart.
     * In order to understate the parallel execution behavior of a parallel stream the next example prints information about the current thread to sout
     */
    private static void parallelStreamExecutionExample(){
        Arrays.asList("a1", "a2", "b1", "c2", "c1")
                .parallelStream()
                .filter(s -> {
                    System.out.format("filter: %s [%s]\n",
                            s, Thread.currentThread().getName());
                    return true;
                })
                .map(s -> {
                    System.out.format("map: %s [%s]\n",
                            s, Thread.currentThread().getName());
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.format("forEach: %s [%s]\n",
                        s, Thread.currentThread().getName()));
    }

    private static void parallelStreamSortMethodExample(){
        //@todo:research and investigate the sort method in parallelStreams !!!!!
    }
}
