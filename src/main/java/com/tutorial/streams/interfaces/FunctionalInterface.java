package com.tutorial.streams.interfaces;

public interface FunctionalInterface {

    void printMe();

    default void printDefaultMessage() {
        System.out.println("You are printing me from the default method implementation from the interface !!!");
    }

    static void printDefaultMessageFromStaticMessage() {
        System.out.println("You are printing me from the default static method implementation from the interface !!!");
    }
}
