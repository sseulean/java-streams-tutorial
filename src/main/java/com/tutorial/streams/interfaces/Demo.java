package com.tutorial.streams.interfaces;

public class Demo {

    public static void main(String args[]) {

        FunctionalInterface functionalInterface = new FunctionalInterfaceImpl();

        functionalInterface.printDefaultMessage();//calling the default method from the interface

        functionalInterface.printMe();//calling the method from the service implementation class

        FunctionalInterface.printDefaultMessageFromStaticMessage();// calling the static method from the interface.It should be regarded as a helper method

    }

}
