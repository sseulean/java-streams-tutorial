package com.tutorial.streams;

/**
 * @todo: implement this in order to demonstrate the usage and the understanding of streams.
 * Advanced Operations:Collect,FlatMap,Reduce
 *
 */
public class StreamsAdvanceOperations {

    public static void main(String args[]){

    }

    private static void collectDemoOperation(){
        //@todo:please research this method and implement it
    }

    private static void flatMapDemoOperation(){
        //@todo:please research this method and implement it
    }

    private static void reduceDemoOperation(){
        //@todo:please research this method and implement it
    }
}
