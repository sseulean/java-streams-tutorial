package com.tutorial.streams;

import java.util.stream.Stream;

public class StreamsProcessingOrder {

    public static void main(String args[]) {

        //An important characteristic of intermediate operations is laziness. Look at this sample where a terminal operation is missing:
        //When executing this code snippet, nothing is printed to the console.
        //That is because intermediate operations will only be executed when a terminal operation is present.
//        Stream.of("d2", "a2", "b1", "b3", "c")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return true;
//                });

        //The order of the result might be surprising. A naive approach would be to execute
        // the operations horizontally one after another on
        // all elements of the stream. But instead each element moves along the chain vertically.
        // The first string "d2" passes filter then forEach, only then the second string "a2" is processed.
//        Stream.of("d2", "a2", "b1", "b3", "c")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("b");
//                })
////                .findAny()
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));
//                .orElse("NONE");

//        Stream.of("d2", "a2", "b1", "b3", "c")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("A");
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));



        //why order matters
        //As you might have guessed both map and filter are called five times for every string in the underlying collection whereas forEach is only called once.
        // We can greatly reduce the actual number of executions if we change the order of the operations, moving filter to the beginning of the chain:
//        Stream.of("d2", "a2", "b1", "b3", "c")
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("A");
//                })
//                .forEach(s -> System.out.println("forEach: " + s));

        //reduce the actual number of executions if we change the order of the operations, moving filter to the beginning of the chain:
        //important for when performing complex method chains with lkarge set of data
//        Stream.of("d2", "a2", "b1", "b3", "c")
//                .filter(s -> {
//                    System.out.println("filter: " + s);
//                    return s.startsWith("a");
//                })
//                .map(s -> {
//                    System.out.println("map: " + s);
//                    return s.toUpperCase();
//                })
//                .forEach(s -> System.out.println("forEach: " + s));

        //sorteStreamWithJava8();
        improvePerformanceForSortMethod();
    }

    /**
     * Special kind of operation.sort is executed as intermediate operation on the streams and than the rest of the intermediate vertical operations
     * . It's a so called stateful operation since in order to sort a collection of elements you have to maintain state during ordering.
     * First, the sort operation is executed on the entire input collection. In other words sorted is executed horizontally.
     * So in this case sorted is called eight times for multiple combinations on every element in the input collection.
     */
    private static void sorteStreamWithJava8() {

        Stream.of("d2", "a2", "b1", "b3", "c")
                .sorted((s1, s2) -> {
                    System.out.printf("sort: %s; %s\n", s1, s2);
                    return s1.compareTo(s2);
                })
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return s.startsWith("a");
                })
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.println("forEach: " + s));

    }

    /**
     * sorted is never been called because filter reduces the input collection to just one element.
     * So the performance is greatly increased for larger input collections.
     */
    private static void improvePerformanceForSortMethod() {
        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return s.startsWith("a");
                })
                .sorted((s1, s2) -> {
                    System.out.printf("sort: %s; %s\n", s1, s2);
                    return s1.compareTo(s2);
                })
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .forEach(s -> System.out.println("forEach: " + s));
    }


}
